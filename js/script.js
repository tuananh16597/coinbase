$(document).ready(function() {
  // $('.mobile-menu').css('display', 'none');
  $('.close-mobile-menu').on('click', function() {
    $('.mobile-menu').css('display', 'none');
  })
  $('.open-mobile-menu').on('click', function() {
    $('.mobile-menu').css('display', 'flex');
  })
  $('.menu-item').hover(function() {
    $($(this).data('target')).css('display', 'block');
  })
  $('.menu-item').mouseleave(function() {
    $($(this).data('target')).css('display', 'none');
  })
  $('#product .dropdown-menu').hover(function() {
    $(this).css('display', 'block');
  })
  $('#product .dropdown-menu').mouseleave(function() {
    $(this).css('display', 'none');
  })
  $(window).scroll(function(){
    var aTop = $('.header').height();
    if($(this).scrollTop()>=aTop){
        $('.fixed-header').delay(10).fadeIn();
    } else {
      $('.fixed-header').delay(10).fadeOut();
    }
  });
    $('.dropdown-click').on('click', function() {
      $($(this).data('target')).toggle();
    })
    $(document).on('click', '.bOYWuT', function() {
      $(this).addClass('DcSwO');
      $(this).removeClass('bOYWuT');
      $($(this).siblings()).removeClass('DcSwO');
      $($(this).siblings()).addClass('bOYWuT');
      $.each($('.my-pill'), function(i, e) {
        $(e).toggleClass('hidden');
      })
    })
    $('.hover-tooltip').hover(function() {
      $($(this).siblings()[0]).toggleClass('active');
    })
    $('.svg-dashboard').on('click', function() {
      $('.svg-dashboard.active').removeClass('active');
      $(this).addClass('active');
      $.each($('.toggle-block'), function(i, e) {
        $(e).addClass("hidden");
      })
      $($(this).data('target')).removeClass('hidden');
    })
    $('.mobile-menu-dashboard').on('click', function() {
      $('.mobile-menu-nav').toggle();
    })

    $('.mobile-profile-dashboard').on('click', function() {
      $('.mobile-profile-nav').toggle();
    })
    $('.business-tab').on('click', function() {
      $('.modal-scrollable').css('display', 'block');
      $('.business-modal').removeClass('hide');
      $('.business-modal').removeClass('fade');
    })
    $('.close').on('click', function() {
      $('.modal-scrollable').css('display', 'none');
      $('.business-modal').addClass('hide');
      $('.business-modal').addClass('fade');
    })
})
